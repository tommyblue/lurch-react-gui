import { TOGGLE_MOVEMENT, DELETE_MOVEMENT, REQUEST_MOVEMENTS, RECEIVE_MOVEMENTS } from '../constants/actionTypes.js';

const toggleMovement = (movement, action) => {
  switch (action.type) {
    case TOGGLE_MOVEMENT:
      return Object.assign({}, movement, { status: 'toggled' });
    case DELETE_MOVEMENT:
      return Object.assign({}, movement, { status: 'deleted' });
    default:
      return movement;
  }
};

const getMovements = (movements, action) => {
  switch (action.type) {
    case REQUEST_MOVEMENTS:
      return [];
    case RECEIVE_MOVEMENTS:
      return action.movements;
    default:
      return movements;
  }
};

const movements = (state = [], action) => {
  switch (action.type) {
    case REQUEST_MOVEMENTS:
    case RECEIVE_MOVEMENTS:
      return getMovements(state, action);
    case TOGGLE_MOVEMENT:
    case DELETE_MOVEMENT:
      return state.map(m => {
        if (m.id === action.id) {
          return Object.assign({}, state, toggleMovement(m, action));
        }
        return m;
      });
    default:
      return state;
  }
};

export default movements;
