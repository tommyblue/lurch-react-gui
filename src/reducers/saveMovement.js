import { SAVE_NEW_MOVEMENT } from '../constants/actionTypes.js';

const saveMovement = (state = {}, action) => {
  switch (action.type) {
    case SAVE_NEW_MOVEMENT:
      return action.showMovementForm;
    default:
      return state;
  }
};

export default saveMovement;
