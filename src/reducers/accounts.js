import { REQUEST_ACCOUNTS, RECEIVE_ACCOUNTS } from '../constants/actionTypes.js';

const getAccounts = (accounts, action) => {
  switch (action.type) {
    case REQUEST_ACCOUNTS:
      return [];
    case RECEIVE_ACCOUNTS:
      return action.accounts;
    default:
      return accounts;
  }
};

const accounts = (state = [], action) => {
  switch (action.type) {
    case REQUEST_ACCOUNTS:
    case RECEIVE_ACCOUNTS:
      return getAccounts(state, action);
    default:
      return state;
  }
};

export default accounts;
