export default (state = 0, action) => {
  if (action.lastFetch) {
    return action.lastFetch;
  }

  return state;
};
