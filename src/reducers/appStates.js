export default (state = { isLoadingMovements: false }, action) => {
  if (action.appStates) {
    return action.appStates;
  }

  return state;
};
