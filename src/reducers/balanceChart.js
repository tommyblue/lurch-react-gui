import { REQUEST_BALANCE, RECEIVE_BALANCE } from '../constants/actionTypes.js';

const balance = (state = {}, action) => {
  switch (action.type) {
    case REQUEST_BALANCE:
      return {};
    case RECEIVE_BALANCE:
      return action.balanceChart;
    default:
      return state;
  }
};

export default balance;
