import { combineReducers } from 'redux';
import appStates from './appStates.js';
import movements from './movements.js';
import lastFetch from './lastUpdates.js';
import accounts from './accounts.js';
import balanceChart from './balanceChart.js';
import showMovementForm from './showMovementForm.js';
import saveMovement from './saveMovement.js';

const lurchApp = combineReducers({
  appStates,
  movements,
  lastFetch,
  accounts,
  balanceChart,
  accountsBalances: (state = []) => state,
  balance: (state = 0.0) => state,
  showMovementForm,
  saveMovement
});

export default lurchApp;
