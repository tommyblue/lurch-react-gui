import { SHOW_MODAL_TRIGGER } from '../constants/actionTypes.js';

const showMovementForm = (state = false, action) => {
  switch (action.type) {
    case SHOW_MODAL_TRIGGER:
      return action.showMovementForm;
    default:
      return state;
  }
};

export default showMovementForm;
