import React, { PropTypes } from 'react';
import BalanceItem from './BalanceItem.jsx';

const BalanceTable = ({ balance, accountsBalances, appStates, onFetchAccounts }) => {
  if (appStates.isLoadingAccounts === true) {
    return (<div>Loading...</div>);
  }
  return (
    <div className="box content">
      <h3>Bilancio: {balance}</h3>
      <table className="table is-narrow is-striped is-bordered">
        <tbody>
          {accountsBalances.map(accountBalance =>
            <BalanceItem
              key={accountBalance.id}
              {...accountBalance}
            />
          )}
        </tbody>
      </table>
      <button className="button is-primary" onClick={() => onFetchAccounts()}>Fetch</button>
    </div>
  );
};

BalanceTable.propTypes = {
  balance: PropTypes.number.isRequired,
  accountsBalances: PropTypes.array.isRequired,
  appStates: PropTypes.object.isRequired,
  onFetchAccounts: PropTypes.func.isRequired,
};

export default BalanceTable;
