import React, { PropTypes } from 'react';

const MovementItem = ({ id, amount, description, date, account_id, onDelete }) => (
  <tr>
    <td>{amount}</td>
    <td>{description}</td>
    <td>{date}</td>
    <td>{account_id}</td>
    <td></td>
    <td>
      <button className="button is-primary"
        onClick={onDelete}
      >Delete me! {id}</button>
    </td>
  </tr>
);

MovementItem.propTypes = {
  id: PropTypes.number.isRequired,
  amount: PropTypes.string.isRequired,
  description: PropTypes.string,
  date: PropTypes.string.isRequired,
  account_id: PropTypes.number.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default MovementItem;
