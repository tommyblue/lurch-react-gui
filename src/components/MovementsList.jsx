import React, { Component, PropTypes } from 'react';
import MovementItem from './MovementItem.jsx';

class MovementsList extends Component {
  constructor(props) {
    super(props);
    this.onMovementDelete = this.props.onMovementDelete.bind(this);
  }

  componentDidMount() {
    this.props.dispatch(this.props.fetchMovements());
  }

  render() {
    if (this.props.appStates.isLoadingMovements === true) {
      return (<div>Loading...</div>);
    }

    return (
      <div className="content">
        <div className="content">
          <div className="heading">
            <h1 className="title">Movimenti</h1>
          </div>
        </div>
        <table className="table is-striped is-bordered">
          <thead>
            <tr>
              <th>Importo</th>
              <th>Descrizione</th>
              <th>Data</th>
              <th>Conto</th>
              <th>Stato</th>
              <th />
            </tr>
          </thead>
          <tbody>
            {this.props.movements.map(movement =>
              <MovementItem
                key={movement.id}
                {...movement}
                onDelete={() => this.onMovementDelete(movement.id)}
              />
            )}
          </tbody>
        </table>
      </div>
    );
  }
}

MovementsList.propTypes = {
  appStates: PropTypes.object.isRequired,
  movements: PropTypes.array.isRequired,
  onMovementDelete: PropTypes.func.isRequired,
  fetchMovements: PropTypes.func.isRequired,
  dispatch: PropTypes.func.isRequired,
};

export default MovementsList;
