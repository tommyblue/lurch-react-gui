import React, { PropTypes } from 'react';
import { Line } from 'react-chartjs-2';

const dataEntry = {
  label: '',
  fill: false,
  lineTension: 0.1,
  borderCapStyle: 'butt',
  borderDash: [],
  borderDashOffset: 0.0,
  borderJoinStyle: 'miter',
  pointBackgroundColor: '#fff',
  pointBorderWidth: 1,
  pointHoverRadius: 5,
  pointHoverBorderWidth: 2,
  pointRadius: 1,
  pointHitRadius: 10,
  data: [],
};

const colors = [
  '151,187,205',
  '90,220,5',
  '205,95,63',
  '195,105,168',
  '153,127,125',
  '91,205,205',
  '66,205,114',
  '205,148,54',
  '173,226,204',
  '255,226,21',
];

const BalanceChart = ({ balanceChart, appStates, onFetchBalance }) => {
  let index = 0;
  if (appStates.isLoadingAccounts === true || typeof balanceChart.data === 'undefined') {
    return (<div>Loading...</div>);
  }
  const data = {
    datasets: [],
  };
  data.labels = balanceChart.labels;
  for (const name in balanceChart.data) {
    const myEntry = Object.assign({}, dataEntry);
    myEntry.data = balanceChart.data[name];
    myEntry.label = name;
    myEntry.backgroundColor = `rgba(${colors[index]},0.4)`;
    myEntry.borderColor = `rgba(${colors[index]},1)`;
    myEntry.pointBorderColor = `rgba(${colors[index]},1)`;
    myEntry.pointHoverBackgroundColor = `rgba(${colors[index]},1)`;
    // pointHoverBorderColor: 'rgba(220,220,220,1)',
    index += 1;
    data.datasets.push(myEntry);
  }

  return (<div className="content">
    <div className="content">
      <div className="heading">
        <h2 className="title">Ultimi movimenti</h2>
      </div>
    </div>
    <Line data={data} />
  </div>);
};

BalanceChart.propTypes = {
  balanceChart: PropTypes.object.isRequired,
  appStates: PropTypes.object.isRequired,
  onFetchBalance: PropTypes.func.isRequired,
};

export default BalanceChart;
