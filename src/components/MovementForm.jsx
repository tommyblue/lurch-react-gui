import React, { Component, PropTypes } from 'react';

class MovementForm extends Component {
  static initialFormValues() {
    return {
      title: '',
    };
  }

  constructor(props) {
    super(props);
    this.toggleModalTrigger = this.props.toggleModalTrigger.bind(this);
    this.state = {
      formValues: MovementForm.initialFormValues(),
    };
  }

  triggerModal() {
    this.props.dispatch(this.toggleModalTrigger(!this.props.showMovementForm));
  }

  saveNewMovement() {
    this.props.dispatch(this.props.saveMovement(...this.state.formValues));
  }

  handleChange(e) {
    this.setState({
      formValues: Object.assign({},
                                this.state.formValues,
                                { [e.target.name]: e.target.value }),
    });
  }

  undoForm() {
    this.setState({ formValues: MovementForm.initialFormValues() });
    this.triggerModal();
  }

  showModal() {
    return (<div className={`modal${this.props.showMovementForm ? ' is-active' : ''}`}>
      <div className="modal-background" />
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">Nuovo movimento</p>
          <button className="delete" onClick={this.triggerModal.bind(this)} />
        </header>
        <section className="modal-card-body">
          <p className="control">
            <label className="label">Tipo</label>
            <label htmlFor="radio-type-incoming" className="radio">
              <input
                id="radio-type-incoming"
                type="radio"
                name="movementType"
                value="Incoming"
                onChange={this.handleChange.bind(this)}
              /> Entrata
            </label>
            <label htmlFor="radio-type-outgoing" className="radio">
              <input
                id="radio-type-outgoing"
                type="radio"
                name="movementType"
                value="Outgoing"
                onChange={this.handleChange.bind(this)}
              /> Uscita
            </label>
          </p>

          <p className="control">
            <input
              className="input"
              type="text"
              placeholder="Descrizione"
              name="title"
              value={this.state.formValues.title}
              onChange={this.handleChange.bind(this)}
            />
          </p>

          <p className="control">
            <input
              className="input"
              type="text"
              placeholder="Importo"
              name="amount"
              onChange={this.handleChange.bind(this)}
            />
          </p>

          <p className="control">
            <input
              className="input"
              type="text"
              placeholder="Data"
              name="date"
              onChange={this.handleChange.bind(this)}
            />
          </p>

          <p className="control">
            <input
              className="input"
              type="text"
              placeholder="Conto"
              name="account"
              onChange={this.handleChange.bind(this)}
            />
          </p>
        </section>

        <footer className="modal-card-foot">
          <button
            className="button is-primary"
            onClick={this.saveNewMovement.bind(this)}
          >Salva</button>
          <button className="button" onClick={this.undoForm.bind(this)}>Annulla</button>
        </footer>
      </div>
    </div>);
  }

  render() {
    return (
      <div>
        { this.showModal() }
        <button
          className="button is-secondary"
          onClick={this.triggerModal.bind(this)}
        >Nuovo movimento</button>
      </div>
    );
  }
}

MovementForm.propTypes = {
  showMovementForm: PropTypes.bool.isRequired,
  toggleModalTrigger: PropTypes.func.isRequired,
  dispatch: PropTypes.func.isRequired,
  saveMovement: PropTypes.func.isRequired,
};

export default MovementForm;
