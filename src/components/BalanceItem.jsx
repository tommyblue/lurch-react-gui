import React, { PropTypes } from 'react';

const BalanceItem = ({ name, balance }) => (
  <tr>
    <td>{name}</td>
    <td>{balance}</td>
  </tr>
);

BalanceItem.propTypes = {
  name: PropTypes.string.isRequired,
  balance: PropTypes.number.isRequired,
};

export default BalanceItem;
