import { connect } from 'react-redux';
import BalanceChart from '../components/BalanceChart.jsx';
import { fetchBalance } from '../actions';

// const getBalancesFromAccounts = (accounts) => (
//   accounts.map(account => (
//     {
//       id: account.id,
//       name: account.title,
//       balance: parseFloat(account.balance),
//     }
//   ))
// );

const mapStateToProps = state => (
  {
    balanceChart: state.balanceChart,
    appStates: state.appStates,
  }
);

const mapDispatchToProps = dispatch => (
  {
    onFetchBalance: () => {
      dispatch(fetchBalance());
    },
  }
);

const BalanceChartContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(BalanceChart);

export default BalanceChartContainer;
