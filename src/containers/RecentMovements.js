import { connect } from 'react-redux';
import MovementsList from '../components/MovementsList.jsx';
import { deleteMovement, fetchMovements } from '../actions';

const getRecentMovements = movements => (
  movements // .filter(m => m.status === 'recent')
);

const mapStateToProps = state => (
  {
    movements: getRecentMovements(state.movements),
    appStates: state.appStates,
  }
);

const mapDispatchToProps = dispatch => (
  {
    dispatch,
    fetchMovements,
    onMovementDelete: (id) => {
      dispatch(deleteMovement(id));
    },
  }
);

const RecentMovements = connect(
  mapStateToProps,
  mapDispatchToProps
)(MovementsList);

export default RecentMovements;
