import { connect } from 'react-redux';
import MovementForm from '../components/MovementForm.jsx';
import { saveMovement, toggleModalTrigger } from '../actions';

const mapStateToProps = state => (
  {
    showMovementForm: state.showMovementForm,
  }
);

const mapDispatchToProps = dispatch => (
  {
    dispatch,
    toggleModalTrigger,
    saveMovement,
  }
);

const NewMovementForm = connect(
  mapStateToProps,
  mapDispatchToProps
)(MovementForm);

export default NewMovementForm;
