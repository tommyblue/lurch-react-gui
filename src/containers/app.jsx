import React from 'react';

import BalanceChartContainer from './BalanceChart.js';
import Balance from './Balance.js';
import RecentMovements from './RecentMovements.js';
import NewMovementForm from './NewMovementForm.js';

const App = function getApp() {
  return (<section className="section">
    <div className="content is-medium">
      <h1>Panoramica finanziaria</h1>
    </div>
    <div className="columns">
      <div className="column is-one-third">
        <Balance />
        <NewMovementForm />
      </div>
      <div className="column">
        <BalanceChartContainer />
      </div>
    </div>
    <div className="columns">
      <div className="column">
        <RecentMovements />
      </div>
    </div>
  </section>);
};

export default App;
