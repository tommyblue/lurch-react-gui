import { connect } from 'react-redux';
import BalanceTable from '../components/BalanceTable.jsx';
import { fetchAccounts } from '../actions';

const getBalancesFromAccounts = accounts => (
  accounts.map(account => (
    {
      id: account.id,
      name: account.title,
      balance: parseFloat(account.balance),
    }
  ))
);

const getBalance = accounts => (
  accounts.reduce((acc, obj) => acc + parseFloat(obj.balance), 0.0)
);

const mapStateToProps = state => (
  {
    balance: getBalance(state.accounts),
    accountsBalances: getBalancesFromAccounts(state.accounts),
    appStates: state.appStates,
  }
);

const mapDispatchToProps = dispatch => (
  {
    onFetchAccounts: () => {
      dispatch(fetchAccounts());
    },
  }
);

const Balance = connect(
  mapStateToProps,
  mapDispatchToProps
)(BalanceTable);

export default Balance;
