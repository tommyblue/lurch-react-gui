import fetch from 'isomorphic-fetch';
import { API_ENDPOINT } from '../constants/index.js';
import { REQUEST_MOVEMENTS, RECEIVE_MOVEMENTS,
         SAVE_NEW_MOVEMENT, SAVE_NEW_MOVEMENT_SUCCESS, SAVE_NEW_MOVEMENT_FAILURE,
         SHOW_MODAL_TRIGGER, TOGGLE_MOVEMENT, DELETE_MOVEMENT } from '../constants/actionTypes.js';

export const toggleModalTrigger = (newStatus) => (
  {
    type: SHOW_MODAL_TRIGGER,
    showMovementForm: newStatus,
  }
);

export const saveMovementStart = () => (
  {
    type: SAVE_NEW_MOVEMENT,
  }
);

export const saveMovementSuccess = (result) => (
  {
    type: SAVE_NEW_MOVEMENT_SUCCESS,
    result,
  }
);

export const saveMovementFailure = (result) => (
  {
    type: SAVE_NEW_MOVEMENT_FAILURE,
    result,
  }
);

export const saveMovement = (title, amount, account, date, movementType) => (
  (dispatch) => {
    dispatch(saveMovementStart());
    return fetch(`${API_ENDPOINT}/movements.json`)
      .then(response => response.json())
      .then(json =>
        dispatch(saveMovementSuccess(json))
      ).catch(response =>
        dispatch(saveMovementFailure(response)));
  }
);

export const toggleMovement = (id) => (
  {
    type: TOGGLE_MOVEMENT,
    id,
  }
);

export const deleteMovement = (id) => (
  {
    type: DELETE_MOVEMENT,
    id,
  }
);

export const requestMovements = () => (
  {
    type: REQUEST_MOVEMENTS,
    movements: [],
    appStates: {
      isLoadingMovements: true,
    },
  }
);

export const receiveMovements = (json) => (
  {
    type: RECEIVE_MOVEMENTS,
    movements: json.movements,
    lastFetch: Date.now(),
    appStates: {
      isLoadingMovements: false,
    },
  }
);

export const fetchMovements = () => (
  (dispatch) => {
    dispatch(requestMovements());
    return fetch(`${API_ENDPOINT}/movements.json`)
      .then(response => response.json())
      .then(json =>
        dispatch(receiveMovements(json))
      );
  }
);
