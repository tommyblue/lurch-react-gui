import fetch from 'isomorphic-fetch';
import { API_ENDPOINT } from '../constants/index.js';
import { REQUEST_BALANCE, RECEIVE_BALANCE } from '../constants/actionTypes.js';

export const requestAccounts = () => (
  {
    type: REQUEST_BALANCE,
    balanceChart: {},
    appStates: {
      loadingBalance: true,
    },
  }
);

export const receiveBalance = json => (
  {
    type: RECEIVE_BALANCE,
    balanceChart: json,
    lastFetch: Date.now(),
    appStates: {
      loadingBalance: false,
    },
  }
);

export const fetchBalance = () => (
  (dispatch) => {
    dispatch(requestAccounts());
    return fetch(`${API_ENDPOINT}/balance.json?num_elements=20`)
      .then(response => response.json())
      .then(json =>
        dispatch(receiveBalance(json))
      );
  }
);
