import fetch from 'isomorphic-fetch';
import { API_ENDPOINT } from '../constants/index.js';
import { REQUEST_ACCOUNTS, RECEIVE_ACCOUNTS } from '../constants/actionTypes.js';

export const requestAccounts = () => (
  {
    type: REQUEST_ACCOUNTS,
    accounts: [],
    appStates: {
      isLoadingAccounts: true,
    },
  }
);

export const receiveAccounts = (json) => (
  {
    type: RECEIVE_ACCOUNTS,
    accounts: json.accounts,
    lastFetch: Date.now(),
    appStates: {
      isLoadingAccounts: false,
    },
  }
);

export const fetchAccounts = () => (
  (dispatch) => {
    dispatch(requestAccounts());
    return fetch(`${API_ENDPOINT}/accounts.json`)
      .then(response => response.json())
      .then(json =>
        dispatch(receiveAccounts(json))
      );
  }
);
