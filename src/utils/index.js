import { DEFAULT_DATETIME } from '../constants';
import { fetchAccounts, fetchBalance } from '../actions';

export const getParsedDatetime = (datetime) => {
  if (datetime instanceof Date) {
    return datetime.toUTCString();
  } else if (typeof datetime === 'number') {
    return (datetime > 0) ? (new Date(datetime)).toUTCString() : DEFAULT_DATETIME;
  }
  return DEFAULT_DATETIME;
};

export const initialState = {
  movements: [],
  accountsBalances: [],
  balance: 0.0,
  balanceChart: {},
  showMovementForm: false,
  appStates: {
    isLoadingMovements: true,
    isLoadingAccounts: true,
  },
};

export const fetchInitialStates = (store) => {
  store.dispatch(fetchAccounts());
  store.dispatch(fetchBalance());
};
