import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';
import { initialState, fetchInitialStates } from './utils';
import App from './containers/app.jsx';
import lurchApp from './reducers';

const loggerMiddleware = createLogger();
const store = createStore(
  lurchApp,
  initialState,
  applyMiddleware(
    thunkMiddleware,
    loggerMiddleware
  ));

fetchInitialStates(store);
render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('app'));
